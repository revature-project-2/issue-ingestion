provider "google" {
  project = "town-hall-service"
  region = "us-central1"
  zone = "us-central1-a"
}
resource "google_pubsub_topic" "issue-topic" {
  name = "issue-topic"
}
resource "google_pubsub_subscription" "issue-subscription" {
  name  = "issue-subscription"
  topic = "${google_pubsub_topic.issue-topic.name}"
}
resource "google_cloud_run_service" "issue-ingestion" {
  name     = "issue-ingestion"
  location = "us-central1"
  template {
    spec {
      containers {
        image = "gcr.io/town-hall-service/ingestion@sha256:0a6ba34b61af48ad275264e5c211b105a76fd7218fc7711cbe6b391bba4d1a07"
      }
    }
  }
}
data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.issue-ingestion.location
  project     = google_cloud_run_service.issue-ingestion.project
  service     = google_cloud_run_service.issue-ingestion.name

  policy_data = data.google_iam_policy.noauth.policy_data
}